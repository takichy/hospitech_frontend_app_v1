DOCKER_COMPOSE_CMD=$(shell echo 'docker-compose')

frontend_install:
	$(DOCKER_COMPOSE_CMD) run --rm server_web_front yarn install

prepare: \
	frontend_install

up:
	$(DOCKER_COMPOSE_CMD) up -d
