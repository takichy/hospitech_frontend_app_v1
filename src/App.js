import React, { Component } from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import AuthService from './services/auth-service';

import Login from './components/login_component';
import Register from './components/register_component';
import RegisterSuperAdmin from './components/register_super_admin_component';
import Home from './components/home_component';
import Profile from './components/profile_component';
import BoardUser from './components/board-user_component';
import UserLicence from './components/user-licence_component';
import BoardManager from './components/board-manager_component';
import BoardAdmin from './components/board-admin_component';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currentUser: '',
            showModeratorBoard: false,
            showAdminBoard: false
        };
    }

    UNSAFE_componentWillReceiveProps() {
        this.logOut = this.logOut.bind(this);
    }

    componentDidMount() {
        // const csrfToken = AuthService.getCsrfToken();
        let showAdminBoard = false;
        AuthService.getCurrentUser().then(response => {
            if (response && response !== 'Bad Token' && response !== undefined && response.user !== null) {
                if (response.user.role === 'ROLE_SUPER_ADMIN') {
                    showAdminBoard = true;
                }
                this.setState({
                    currentUser: response,
                    showModeratorBoard: false,
                    showAdminBoard
                });
            }
            if (
                (typeof response === 'string' && response.includes('Bad Token') && localStorage.getItem('token')) ||
                (response.user === null && localStorage.getItem('token'))
            ) {
                localStorage.removeItem('token');
                localStorage.removeItem('csrf');
                return (window.location.href = '/');
            }
        });
    }

    logOut() {
        AuthService.logout();
    }

    render() {
        const { currentUser, showModeratorBoard, showAdminBoard } = this.state;
        return (
            <div>
                <nav className="navbar navbar-expand navbar-dark bg-dark">
                    <Link to={'/'} className="navbar-brand">
                        Hospitech
                    </Link>
                    <div className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <Link to={'/home'} className="nav-link">
                                Home
                            </Link>
                        </li>

                        {showModeratorBoard && (
                            <li className="nav-item">
                                <Link to={'/mod'} className="nav-link">
                                    Moderator Board
                                </Link>
                            </li>
                        )}

                        {/* {showModeratorBoard && (
                            <li className="nav-item">
                                <Link to={'/mod'} className="nav-link">
                                    Moderator Board
                                </Link>
                            </li>
                        )} */}

                        {showAdminBoard && (
                            <li className="nav-item">
                                <Link to={'/admin'} className="nav-link">
                                    Admin Board
                                </Link>
                            </li>
                        )}

                        {currentUser && (
                            <>
                                <li className="nav-item">
                                    <Link to={'/profile'} className="nav-link">
                                        User
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link to={'/licence'} className="nav-link">
                                        Licence
                                    </Link>
                                </li>
                            </>
                        )}
                        {currentUser.user && currentUser.user.licence && currentUser.user.licence === 'urgence_only' ? (
                            <li className="nav-item">
                                <Link to={'/Urgence'} className="nav-link">
                                    Centre d&rsquo;urgences {/*`&rsquo;` == apostrophe */}
                                </Link>
                            </li>
                        ) : (
                            <></>
                        )}

                        {currentUser.user && currentUser.user.licence && currentUser.user.licence === 'logistic_only' ? (
                            <li className="nav-item">
                                <Link to={'/Logistic'} className="nav-link">
                                    Vos réglages logistiques {/*`&rsquo;` == apostrophe */}
                                </Link>
                            </li>
                        ) : (
                            <></>
                        )}

                        {currentUser.user && currentUser.user.role && currentUser.user.role === 'ROLE_USER' ? (
                            <li className="nav-item">
                                <Link to={'/admin'} className="nav-link">
                                    Administration
                                </Link>
                            </li>
                        ) : (
                            <></>
                        )}
                    </div>

                    <div className="navbar-nav ml-auto">
                        {currentUser ? (
                            <>
                                <li className="nav-item">
                                    <Link to={'/profile'} className="nav-link">
                                        {currentUser.usermail}
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <a href="/login" className="nav-link" onClick={this.logOut}>
                                        LogOut
                                    </a>
                                </li>
                            </>
                        ) : (
                            <>
                                <li className="nav-item">
                                    <Link to={'/login'} className="nav-link">
                                        Login
                                    </Link>
                                </li>

                                <li className="nav-item">
                                    <Link to={'/register'} className="nav-link">
                                        Sign Up
                                    </Link>
                                </li>
                            </>
                        )}
                    </div>
                </nav>

                <div className="container mt-3">
                    <Switch>
                        <Route exact path={['/', '/home']} component={Home} />
                        <Route exact path="/login" component={Login} />
                        <Route exact path="/register" component={Register} />
                        <Route exact path="/profile" component={Profile} />
                        <Route path="/user" component={BoardUser} />
                        <Route path="/licence" component={UserLicence} />
                        <Route path="/mod" component={BoardManager} />
                        <Route path="/admin" component={BoardAdmin} />
                        <Route path="/create_super_admin" component={RegisterSuperAdmin} />
                        <Route path="/Logistic" component={BoardAdmin} />
                    </Switch>
                </div>
            </div>
        );
    }
}

export default App;

// import logo from './logo.svg';
// import './App.css';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;
