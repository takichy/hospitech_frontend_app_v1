import React, { Component } from 'react';
import AuthService from '../services/auth-service';

export default class Profile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currentUser: null
        };
    }

    componentDidMount() {
        const csrfToken = AuthService.getCsrfToken();
        const olduser = AuthService.getCurrentUser();

        olduser.then(async () => {
            const user = await olduser;
            if (user && user !== 'Bad Token' && user !== undefined) {
                this.setState({
                    currentUser: user,
                    showModeratorBoard: [''], //user.roles.includes("ROLE_MODERATOR"),
                    showAdminBoard: [''] //user.roles.includes("ROLE_ADMIN"),
                });
            }
            if (user === 'Bad Token') {
                /* eslint-disable react/prop-types */ // TODO: upgrade to latest eslint tooling
                this.props.history.push('/home');
                window.location.reload();
            }
        });

        csrfToken.then(async () => {
            // console.log(await csrfToken);
        });
    }

    render() {
        const { currentUser } = this.state;
        return (
            <div className="container">
                <header className="jumbotron">
                    <h3>
                        <strong>{currentUser ? `${currentUser.user.firstname} - ${currentUser.user.lastname}` : 'Error'}</strong>{' '}
                        Profile
                    </h3>
                </header>
                <p>
                    {/* <strong>Token:</strong>{" "} */}
                    {/* {currentUser.accessToken.substring(0, 20)} ...{" "}
          {currentUser.accessToken.substr(currentUser.accessToken.length - 20)} */}
                </p>
                <p>
                    <strong>Id:</strong> {currentUser ? currentUser.user._id : 'Error'}
                </p>
                <p>
                    <strong>Email:</strong> {currentUser ? currentUser.user.email : 'Error'}
                </p>
                <p>
                    <strong>Authorities: </strong>
                    {/* <ul> */}
                    {currentUser ? currentUser.user.role : 'Error'}
                    {/* {currentUser.user.role &&
            currentUser.role.map((role, index) => <li key={index}>{role}</li>)} */}
                    {/* </ul> */}
                </p>
                <p>
                    <strong>Licence: </strong>
                    {currentUser ? currentUser.user.licence : 'Error'}
                </p>
            </div>
        );
    }
}
