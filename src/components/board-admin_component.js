import React, { Component, Fragment } from 'react';
import { Modal, Button } from 'react-bootstrap';
import { withScriptjs, withGoogleMap, GoogleMap
    , Marker 
} from 'react-google-maps';
import HospiService from '../services/hospi-service';
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete';

export default class BoardAdmin extends Component {
    // static defaultProps = {
    //     googleMapURL:
    //         'https://maps.googleapis.com/maps/api/js?key=AIzaSyBmvaByszp1whxVtUduqCnGI0u1CmMWu_Y&v=3.exp&libraries=geometry,drawing,places'
    // };
    constructor(props) {
        super(props);

        this.state = {
            myhospitals: [],
            showAddHospital: false,
            markerLatitude: 0,
            markerLongitude: 0,
            markerLongitudeList: 0,
            markerLatitudeList: 0,
            centerName: "",
            pictureCenter: "",
            googleMapURL:
                'https://maps.googleapis.com/maps/api/js?key=AIzaSyBmvaByszp1whxVtUduqCnGI0u1CmMWu_Y&v=3.exp&libraries=geometry,drawing,places',
            address: ''
        };

        this.showAddHospitalModal = this.showAddHospitalModal.bind(this);
        this.hideAddHospitalModal = this.hideAddHospitalModal.bind(this);
        this.hideAddHospitalAndSaveModal = this.hideAddHospitalAndSaveModal.bind(this);
        this.onChangeCenterName = this.onChangeCenterName.bind(this);
        this.onChangeCenterPicture = this.onChangeCenterPicture.bind(this);
        this.handleDeleteClick = this.handleDeleteClick.bind(this);
    }

    onChangeCenterName = (e) => {
        this.setState({
            centerName: e.target.value
        });
    }

    convertBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file)
            fileReader.onload = () => {
                resolve(fileReader.result);
            }
            fileReader.onerror = (error) => {
                reject(error);
            }
        })
    }

    onChangeCenterPicture = async (event) => {
        const file = event.target.files[0]
        const base64 = await this.convertBase64(file)
        this.setState({
            pictureCenter: base64
        });
    }

    CMap = withScriptjs(
        withGoogleMap(() => (
            <GoogleMap defaultZoom={4} defaultCenter={{ lat: 48.864, lng: 2.349 }} onClick={this.handleClickedMap }  >
                {this.state.markerLatitude != 0 && this.state.markerLongitude != 0 ? (<>
                    <Marker position={{ lat: this.state.markerLatitude, lng: this.state.markerLongitude }} />
                </>) : (<></>) }
            </GoogleMap>
        ))
    );
 
    CMapList = withScriptjs(
        withGoogleMap((props) => (
            console.log("Propz =>", props),
            <GoogleMap defaultZoom={10} defaultCenter={{ lat: props.latitude, lng: props.longitude }}  >
                {props.latitude && props.longitude ? (<>
                    <Marker position={{ lat: props.latitude, lng: props.longitude }} />
                </>) : (<></>) }
            </GoogleMap>
        ))
    );

    handleClickedMap = (e) => {
        let latitude = e.latLng.lat();
        let longtitude  = e.latLng.lng();
        this.setState({ markerLatitude: latitude, markerLongitude: longtitude });
    }

    handleChangeInputEmplacement = (e) => {
        this.setState({ address: e });
        console.log("Change input =>", e);
    }

    handleSelectInputEmplacement = (e) => {
        geocodeByAddress(e)
        .then(results => getLatLng(results[0]))
        .then(({ lat, lng }) =>
            this.setState({ markerLatitude: lat, markerLongitude: lng })
        );
    }

    showAddHospitalModal = () => {
        this.setState({ showAddHospital: true });
    };

    hideAddHospitalModal = () => {
        this.setState({ showAddHospital: false });
    };

    hideAddHospitalAndSaveModal = () => {
        this.setState({ showAddHospital: false });
        HospiService.createOne(this.state.markerLatitude, this.state.markerLongitude, this.state.centerName, this.state.pictureCenter).then(
            async () => {
                let allHospis = await HospiService.getAll();
                this.setState({
                    myhospitals: allHospis
                });        
            },
        );
    };

    handleDeleteClick = (e) => {
        HospiService.deleteOne(e.target.id).then(
            async (e) => {
                console.log("result =>", e);
                if (e == 200) {
                    let allHospis = await HospiService.getAll();
                    this.setState({
                        myhospitals: allHospis
                    });
                }
            },
        );
    };

    componentDidMount() {
        HospiService.getAll().then(
            (e) => {
                this.setState({
                    myhospitals: e
                });
                /* eslint-disable react/prop-types */ // TODO: upgrade to latest eslint tooling
                // window.location.reload();
            },
            // error => {
            //     // const resMessage =
            //     //     (error.response && error.response.data && error.response.data.message) || error.message || error.toString();
            // }
        );
    }

    render() {
        return (
            <div className="container">
                <div className="row justify-content-md-center">
                    <div className="col col-md-auto">
                        <div className="text-center mt-2">
                            <h3>
                                <b> Interface de gestion </b>
                            </h3>
                        </div>
                    </div>
                </div>
                <br />
                <div className="row my-5">
                    <div className="col">
                        <div className="container border rounded-25" style={{ background: 'rgba(225, 225, 225, .8)' }}>
                            <div className="overflow-auto">
                                <div className="col text-center">
                                    <br />
                                    <button type="button" className="my-2 btn btn-primary" onClick={this.showAddHospitalModal}>
                                        Ajouter un centre d urgence
                                    </button>
                                    <br />
                                    <br />
                                    <h4>
                                        Vos centres :
                                    </h4>
                                    <hr />
                                    <Modal show={this.state.showAddHospital} onHide={this.hideAddHospitalModal} animation={false}>
                                        <Modal.Header closeButton>
                                            <Modal.Title> Ajouter un nouveau centre : </Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>
                                            <form>
                                                <div className="input-group">
                                                    <div className="mb-3 row justify-content-md-center">
                                                        <label htmlFor="hospiname" className="col-form-label">
                                                            {' '}
                                                            Nom du centre :{' '}
                                                        </label>
                                                        <div className="col-sm-7">
                                                            <input
                                                                type="text"
                                                                name="hospiname"
                                                                id="hospiname"
                                                                // value={}
                                                                // onChange={}
                                                                title="Hospital Name"
                                                                className="form-control-plaintext border rounded"
                                                                required
                                                                onChange={this.onChangeCenterName}
                                                            />
                                                        </div>
                                                    </div>

                                                    <div className="row mb-3">
                                                        <label htmlFor="formFile" className="col-sm-2 col-form-label">
                                                            Photo
                                                        </label>
                                                        <div className="col-sm-10">
                                                            <input className="form-control" type="file" id="formFile" onChange={this.onChangeCenterPicture}/>
                                                        </div>
                                                    </div>

                                                    <div className="row mb-3">
                                                        <div>
                                                        <label htmlFor="formFile" className="col-sm-2 col-form-label" style={{ width: '100%'}}>
                                                            Emplacement:
                                                        </label>
                                                        </div>
                                                        <PlacesAutocomplete
                                                            value={this.state.address}
                                                            onChange={this.handleChangeInputEmplacement}
                                                            onSelect={this.handleSelectInputEmplacement}
                                                        >
                                                            {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                                                            <div>
                                                                <input
                                                                {...getInputProps({
                                                                    placeholder: 'Search Places ...',
                                                                    className: 'location-search-input',
                                                                })}
                                                                />
                                                                <div className="autocomplete-dropdown-container">
                                                                {loading && <div>Loading...</div>}
                                                                {suggestions.map((suggestion, idx) => {
                                                                    const className = suggestion.active
                                                                    ? 'suggestion-item--active'
                                                                    : 'suggestion-item';
                                                                    // inline style for demonstration purpose
                                                                    const style = suggestion.active
                                                                    ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                                                    : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                                                    return (
                                                                    <div key={idx}
                                                                        {...getSuggestionItemProps(suggestion, {
                                                                        className,
                                                                        style,
                                                                        })}
                                                                    >
                                                                    <span>{suggestion.description}</span>
                                                                    </div>
                                                                    );
                                                                })}
                                                                </div>
                                                            </div>
                                                            )}
                                                        </PlacesAutocomplete>
                                                        <div className="container">
                                                            {/* <input className="form-control" type="file" id="formFile" /> */}{' '}
                                                            {/* form google */}
                                                            <Fragment>
                                                                {this.state && this.state.googleMapURL ? (
                                                                    <this.CMap
                                                                        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAC2lMOTkt90cBBo3mZ1D-fzok9MgekNvg&libraries=geometry,drawing,places"
                                                                        loadingElement={<div style={{ height: `100%` }} />}
                                                                        containerElement={
                                                                            <div style={{ height: `300px`, width: `400px` }} />
                                                                        }
                                                                        mapElement={<div style={{ height: `100%` }} />}
                                                                    />
                                                                ) : (
                                                                    <></>
                                                                )}
                                                            </Fragment>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </Modal.Body>
                                        <Modal.Footer>
                                            <Button variant="secondary" onClick={this.hideAddHospitalModal}>
                                                Close
                                            </Button>
                                            <Button variant="primary" onClick={this.hideAddHospitalAndSaveModal}>
                                                Save Changes
                                            </Button>
                                        </Modal.Footer>
                                    </Modal>
                                </div>
                                {this.state.myhospitals.map((d, idx) => {
                                    return (
                                        <div className="card mb-3 rounded-25" style={{maxWidth: "90%"}} key={idx}>
                                        <div className="row no-gutters">
                                            <div className="col-md-2 ">
                                                <img src={d.picturecenter} className="card-img mb-n5 rounded-25" style={{maxWidth: "85%", maxHeight: "85%"}} alt="..."/>
                                            </div>
                                            <div className="col-md-6">
                                            <div className="card-body">
                                                <h5 className="card-title"> Centre : { d.centername } </h5>
                                                <p className="card-text">nombre de lits disponible : 0</p>
                                                <p className="card-text">nombre de ressources disponibles : 0</p>
                                                <p className="card-text">nombre de produits à bientôt commander : 0</p>
                                                <p className="card-text"><small className="text-muted">Dernière modification 3 min...</small></p>
                                                <br/>
                                                <div className='row d-flex justify-content-around' style={{ width: "75%"}}>
                                                    <button type="button" className="btn btn-info" id={ d._id } >Editer</button>
                                                    <button type="button" className="btn btn-danger"  id={ d._id } onClick={this.handleDeleteClick}>Supprimer</button>
                                                </div>
                                            </div>
                                            </div>
                                            <div className='col-md-4'>
                                                <Fragment>
                                                    {
                                                    // this.setState({
                                                    //     markerLatitudeList: d.markerlatitude,
                                                    //     markerLongitudeList: d.markerlongitude
                                                    // })
                                                    }
                                                    {this.state && this.state.googleMapURL ? (
                                                        <this.CMapList
                                                        latitude={d.markerlatitude}
                                                        longitude={d.markerlongitude}
                                                        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8rkONtazCYE4NyVG6RRzuDDUb5uyMSCA&v=3.exp&libraries=geometry,drawing,places"
                                                            loadingElement={<div style={{ height: `100%` }} />}
                                                            containerElement={
                                                                <div style={{ height: `300px`, width: `auto` }} />
                                                            }
                                                            mapElement={<div style={{ height: `100%` }} />}
                                                        />
                                                    ) : (
                                                        <></>
                                                    )}
                                                </Fragment>
                                            </div>
                                        </div>
                                        </div>
                                    // <li key={idx}>{d.centername} <img src={d.picturecenter} /> </li>
                                    )
                                })}
                            </div>
                        </div>
                    </div>
                    {/* <div className="col">2 of 3</div>
                    <div className="col">3 of 3</div> */}
                </div>
            </div>
        );
    }
}
