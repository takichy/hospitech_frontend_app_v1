import React, { Component } from 'react';

import UserService from '../services/user-service';

export default class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            content: [],
            contentID: []
        };
    }

    componentDidMount() {
        let arrayFirstIter = false;
        UserService.getPublicContent().then(
            response => {
                const arrUser = response.data.list.users;
                arrUser.forEach(element => {
                    if (!arrayFirstIter) {
                        // Sinon on garde l'array [0] vide et ça concate avec les autres index
                        this.setState({
                            content: [element.firstname],
                            contentID: [element._id]
                        });
                        arrayFirstIter = true;
                    } else {
                        // ts-lint ignore-text
                        const tmpArray = this.state.content;
                        tmpArray.push(element.firstname);
                        this.setState({
                            content: tmpArray,
                            contentID: [this.state.contentID, element._id]
                        });
                    }
                });
            },
            error => {
                this.setState({
                    content: (error.response && error.response.data) || error.message || error.toString()
                });
            }
        );
    }

    render() {
        let namesList;
        if (typeof this.state.content !== 'string' && this.state.content !== undefined) {
            console.log('Index ==', this.state.content);
            namesList = this.state.content.map((name, index) => {
                return <span key={index}>{name}</span>;
            });
        }

        return (
            <div className="container">
                <header className="jumbotron">
                    <h3>
                        {' '}
                        Here are the people registered{' '}
                        {namesList !== undefined && namesList.length !== 0 ? (
                            namesList.map((name, index) => {
                                return <li key={index}>{name}</li>;
                            })
                        ) : (
                            <li key={0}>NOoo</li>
                        )}{' '}
                    </h3>
                </header>
            </div>
        );
    }
}
