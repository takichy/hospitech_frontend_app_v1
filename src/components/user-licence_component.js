import React, { Component } from 'react';
import ReactPayPal from './ReactPayPal';
import { Check2Square } from 'react-bootstrap-icons';

import UserService from '../services/user-service';

export default class UserLicence extends Component {
    constructor(props) {
        super(props);

        this.state = {
            content: '',
            userLicence: {}
        };
    }

    componentDidMount() {
        console.log('nice ?');
        UserService.getLicenceBoard().then(
            response => {
                if (response.status === 200) {
                    // let licence = response.data.content.licence;
                    const licence = response.data.content.licence;
                    switch (licence) {
                        case 'URGENCE_LOGISTIQUE':
                            break;
                        case 'URGENCE':
                            break;
                        case 'LOGISTIQUE':
                            break;
                        default:
                            this.setState({
                                content: ''
                            });
                            break;
                    }
                }
                this.setState({
                    userLicence: response.data.content.licence
                });
            },
            error => {
                this.setState({
                    content:
                        (error.response && error.response.data && error.response.data.message) || error.message || error.toString()
                });
            }
        );
    }

    render() {
        return (
            <div className="container">
                <header className="jumbotron text-center">
                    {/* <h3>{this.state.content}</h3> */}
                    <h3> Tarification de la solution Hospitech</h3>
                </header>
                <div className="row text-center">
                    <div className="col border rounded pricePanel mr-3">
                        <div className="container">
                            <br />
                            PACK LOGISTIQUE :
                            <ul style={{ listStyleType: 'none', overflow: 'auto' }}>
                                <br />
                                <li className="text-left" style={{ width: '100%' }}>
                                    🗸 Gestion des stock
                                </li>
                                <hr />
                                <li className="text-left" style={{ width: '100%' }}>
                                    🗸 Alerte en cas de pénurie
                                </li>
                                <hr />
                                <li className="text-left" style={{ width: '100%' }}>
                                    🗸 Alerte sur mobile et tablette
                                </li>
                                <hr />
                                <li className="text-left" style={{ width: '100%' }}>
                                    🗸 Commande automatique chez votre fournisseur (si le fournisseur le permet)
                                </li>
                                <hr />
                                <li className="text-left" style={{ width: '100%' }}>
                                    🗸 Prediction des besoins en stock
                                </li>
                                <hr />
                            </ul>
                            <br />
                            <div style={{ marginTop: '8vh' }}>
                                {this.state.userLicence === 'logistic_only' ? (
                                    <>
                                        <img
                                            style={{ color: 'green' }}
                                            src="https://img.icons8.com/external-flatart-icons-outline-flatarticons/64/000000/external-check-basic-ui-elements-flatart-icons-outline-flatarticons.png"
                                        />
                                        <i className="bi bi-check2-square"></i>
                                        <br />
                                        <button type="button" className="btn btn-danger">
                                            Cancel
                                        </button>
                                    </>
                                ) : (
                                    <>
                                        Price :
                                        <ReactPayPal total={45} licence={'logistique'} currency={'EUR'} />
                                    </>
                                )}
                            </div>
                        </div>
                    </div>
                    <div className="col border rounded pricePanel mr-3">
                        <div className="container">
                            <br />
                            PACK URGENCE :
                            <ul style={{ listStyleType: 'none', overflow: 'auto' }}>
                                <br />
                                <li className="text-left" style={{ width: '100%' }}>
                                    🗸 Gestion des arrivées
                                </li>
                                <hr />
                                <li className="text-left" style={{ width: '100%' }}>
                                    🗸 Alerte en temps sur le risque de saturation (trop plein) sur mobile et tablette
                                </li>
                                <hr />
                                <li className="text-left" style={{ width: '100%' }}>
                                    🗸 Redirection du trop plein sur les centres plus proches dans les alentours
                                </li>
                                <hr />
                                <li className="text-left" style={{ width: '100%' }}>
                                    🗸 Prediction des arrivées jusqu `&apos` à 2 jours
                                </li>
                                <hr />
                            </ul>
                            <br />
                            <div style={{ marginTop: '6vh' }}>
                                {this.state.userLicence === 'urgence_only' ? (
                                    <>
                                        <Check2Square color="green" size={96} />
                                        <br />
                                        <br />
                                        <button type="button" className="btn btn-danger">
                                            Cancel
                                        </button>
                                    </>
                                ) : (
                                    <>
                                        Price :
                                        <ReactPayPal total={65} licence={'urgence'} currency={'EUR'} />
                                    </>
                                )}
                            </div>
                        </div>
                    </div>
                    <div className="col border rounded pricePanel">
                        <div className="container">
                            <br />
                            PACK COMBO :
                            <ul style={{ listStyleType: 'none', overflow: 'auto' }}>
                                <br />
                                <li className="text-left" style={{ width: '100%' }}>
                                    <p>
                                        🗸 Gestion des arrivées
                                        <br />
                                        🗸 Gestion des stock
                                    </p>
                                </li>
                                <hr />
                                <li className="text-left" style={{ width: '100%' }}>
                                    <p>
                                        🗸 Alerte en cas de pénurie
                                        <br />
                                        🗸 Alerte en temps sur le risque de saturation
                                    </p>
                                </li>
                                <hr />
                                <li className="text-left" style={{ width: '100%' }}>
                                    🗸 Alerte sur mobile et tablette
                                </li>
                                <hr />
                                <li className="text-left" style={{ width: '100%' }}>
                                    🗸 Commande automatique chez votre fournisseur (si le fournisseur le permet)
                                </li>
                                <hr />
                                <li className="text-left" style={{ width: '100%' }}>
                                    🗸 Prediction des besoins en stock
                                </li>
                                <hr />
                            </ul>
                            <br />
                            <div>
                                {this.state.userLicence === 'combo_only' ? (
                                    <>
                                        <img
                                            style={{ color: 'green' }}
                                            src="https://img.icons8.com/external-flatart-icons-outline-flatarticons/64/000000/external-check-basic-ui-elements-flatart-icons-outline-flatarticons.png"
                                        />
                                        <i className="bi bi-check2-square"></i>
                                        <br />
                                        <button type="button" className="btn btn-danger">
                                            Cancel
                                        </button>
                                    </>
                                ) : (
                                    <>
                                        Price :
                                        <ReactPayPal total={100} licence={'combo'} currency={'EUR'} />
                                    </>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
