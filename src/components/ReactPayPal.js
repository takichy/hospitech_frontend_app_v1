import React from 'react';
import UserService from '../services/user-service';
import PropTypes from 'prop-types';

export default function ReactPayPal(props) {
    const [paid, setPaid] = React.useState(false);
    const [error] = React.useState(null);
    const paypalRef = React.useRef();

    // To show PayPal buttons once the component loads
    React.useEffect(() => {
        window.paypal
            .Buttons({
                createOrder: (data, actions) => {
                    return actions.order.create({
                        intent: 'CAPTURE',
                        purchase_units: [
                            {
                                description: `Licence ${props.licence}`,
                                amount: {
                                    currency_code: 'EUR',
                                    value: props.total
                                }
                            }
                        ]
                    });
                },
                onApprove: async (data, actions) => {
                    const order = await actions.order.capture();
                    setPaid(true);
                    if (order.status === 'COMPLETED') {
                        UserService.setOrder(order);
                    }
                },
                onError: err => {
                    //   setError(err),
                    console.error(err);
                }
            })
            .render(paypalRef.current);
    }, []);

    // If the payment has been made
    if (paid) {
        return <div>Payment successful.!</div>;
    }

    // If any error occurs
    if (error) {
        return <div>Error Occurred in processing payment.! Please try again.</div>;
    }

    return (
        <div>
            <h5>
                {' '}
                {props.currency} : {props.total} per month
            </h5>
            <br />
            <h6>
                {' '}
                or annualy : {props.total * 12 - (props.total * 12) / 10} {props.currency}{' '}
            </h6>
            <div ref={paypalRef} />
        </div>
    );
}

ReactPayPal.propTypes = {
    licence: PropTypes.string,
    total: PropTypes.number,
    currency: PropTypes.string
};
