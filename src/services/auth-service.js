import axios from 'axios';

const SERVER = process.env.REACT_APP_API_SERVER ? process.env.REACT_APP_API_SERVER : process.env.REACT_APP_API_SERVER_LOCAL;

const API_URL = `http://${SERVER}:${process.env.REACT_APP_API_PORT}`;

class AuthService {
    login(email, password) {
        return axios
            .post(`${API_URL + /users/}login`, {
                email,
                password
            })
            .then(response => {
                if (response.data.content.token) {
                    localStorage.setItem('token', response.data.content.token);
                }

                return response.data;
            });
    }

    logout() {
        localStorage.removeItem('token');
    }

    register(email, firstname, lastname, password, role_set) {
        const role = role_set ? role_set : 'ROLE_USER';
        console.log('Url ==>', `${API_URL}/users/register`);
        return axios.post(`${API_URL}/users/register`, {
            // username,
            email,
            password,
            firstname,
            lastname,
            role
        });
    }

    async getCurrentUser() {
        const token = localStorage.getItem('token');
        const csrfToken = localStorage.getItem('csrf');

        if (token && csrfToken) {
            try {
                const data = await axios.get(`${API_URL}/users/profile`, {
                    headers: {
                        'Content-Type': 'application/json',
                        Accept: 'application/json',
                        'CSRF-Token': csrfToken,
                        'X-CSRF-Token': csrfToken,
                        Authorization: `Bearer ${token}`
                    }
                });

                if (data.status === 200) {
                    return data.data.content;
                } else {
                    // // if (error.response.data.message == "Error user is not logged.") {
                    // // }
                    return 'Bad Token';
                }
            } catch (err) {
                return 'Bad Token';
            }
        } else {
            return 'Bad Token';
        }
    }

    async getCsrfToken() {
        try {
            const data = await axios.get(`${API_URL}/csrf`, {});

            if (data.status === 200) {
                if (data.data.content.csrfToken) {
                    localStorage.setItem('csrf', data.data.content.csrfToken);
                    return 'Good Token';
                }
            } else {
                return 'Bad Token';
            }
        } catch (err) {
            return 'Bad Token';
        }
    }
}

export default new AuthService();
