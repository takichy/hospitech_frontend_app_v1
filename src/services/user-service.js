import axios from 'axios';
import authHeader from './auth-header';

// const SERVER = process.env.REACT_APP_API_SERVER ? process.env.REACT_APP_API_SERVER : process.env.REACT_APP_API_SERVER_LOCAL;

const API_URL = `http://${process.env.REACT_APP_API_SERVER_LOCAL}:${process.env.REACT_APP_API_PORT}`;

class UserService {
    getPublicContent() {
        return axios.get(`${API_URL}/users`);
    }

    async setOrder(order) {
        const token = localStorage.getItem('token');
        let newLicence;
        switch (order.purchase_units[0].description) {
            case 'Licence logistique':
                newLicence = 'logistic_only';
                break;
            case 'Licence urgence':
                newLicence = 'urgence_only';
                break;
            case 'Licence combo':
                newLicence = 'combo_only';
                break;
            default:
                newLicence = '';
                break;
        }

        if (token) {
            try {
                const headers = authHeader();
                const data = {
                    orders: order,
                    licence: newLicence
                };
                const result = await axios.put(`${API_URL}/users/update_licence`, data, {
                    headers
                });

                if (result.status === 200) {
                    return result.data.content;
                } else {
                    // // if (error.response.data.message == "Error user is not logged.") {
                    // // }
                    return 'Bad Token';
                }
            } catch (err) {
                return 'Bad Token';
            }
        } else {
            return 'Bad Token';
        }
    }

    getUserBoard() {
        console.log('User Url ==>', API_URL);
        return axios.get(`${API_URL}/users/user`, { headers: authHeader() });
    }

    getManagerBoard() {
        return axios.get(`${API_URL}/man`, { headers: authHeader() });
    }

    getAdminBoard() {
        return axios.get(`${API_URL}/admin`, { headers: authHeader() });
    }

    getLicenceBoard() {
        return axios.get(`${API_URL}/users/licence`, { headers: authHeader() });
    }

    // updateLicenceBoard(update_licence) {
    //      return axios.put(API_URL + '/users/licence', { headers: authHeader() });
    // }
}

export default new UserService();
