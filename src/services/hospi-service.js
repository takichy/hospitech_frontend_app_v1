import axios from 'axios';

const SERVER = process.env.REACT_APP_API_SERVER ? process.env.REACT_APP_API_SERVER : process.env.REACT_APP_API_SERVER_LOCAL;

const API_URL = `http://${SERVER}:${process.env.REACT_APP_API_PORT}`;

class HospiService {
    async createOne(markerLatitude, markerLongitude, centerName, pictureCenter) {
        const token = localStorage.getItem('token');
        const csrfToken = localStorage.getItem('csrf');

        if (token && csrfToken) {
            try {
                const data = await axios.post(`${API_URL}/hospital/create`, {
                    markerlatitude: markerLatitude,
                    markerlongitude: markerLongitude,
                    centername: centerName,
                    picturecenter: pictureCenter
                }, {
                    headers: {
                        'Content-Type': 'application/json',
                        Accept: 'application/json',
                        'CSRF-Token': csrfToken,
                        'X-CSRF-Token': csrfToken,
                        Authorization: `Bearer ${token}`
                    }
                    });

                if (data.status === 200) {
                    return data.data.content;
                } else {
                    return data.statusText;
                }
            } catch (err) {
                return 'a Bad Token while creating hospital';
            }
        } else {
            return 'Bad Token in call';
        }
    }

    async getAll() {
        const token = localStorage.getItem('token');
        const csrfToken = localStorage.getItem('csrf');

        if (token && csrfToken) {
            try {
                const data = await axios.get(`${API_URL}/hospital`, {
                    headers: {
                        'Content-Type': 'application/json',
                        Accept: 'application/json',
                        'CSRF-Token': csrfToken,
                        'X-CSRF-Token': csrfToken,
                        Authorization: `Bearer ${token}`
                    }
                    });

                if (data.status === 200) {
                    return data.data.hospitals;
                } else {
                    return data.statusText;
                }
            } catch (err) {
                return 'a Bad Token while creating hospital';
            }
        } else {
            return 'Bad Token in call';
        }
    }

    async deleteOne(hospiId) {
        const token = localStorage.getItem('token');
        const csrfToken = localStorage.getItem('csrf');

        if (token && csrfToken) {
            try {
                const data = await axios.delete(`${API_URL}/hospital/${hospiId}`, {
                    headers: {
                        'Content-Type': 'application/json',
                        Accept: 'application/json',
                        'CSRF-Token': csrfToken,
                        'X-CSRF-Token': csrfToken,
                        Authorization: `Bearer ${token}`
                    }
                    });
                if (data.status === 200) {
                    return data.status;
                } else {
                    return null;
                }
            } catch (err) {
                return 'a Bad Token while creating hospital';
            }
        } else {
            return 'Bad Token in call';
        }
    }
}

export default new HospiService();
