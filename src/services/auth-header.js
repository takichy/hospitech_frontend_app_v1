export default function authHeader() {
    const user = localStorage.getItem('token');
    // let csrfToken = localStorage.getItem('csrf');
    const csrfToken = localStorage.getItem('csrf');

    if (user) {
        // for Node.js Express back-end
        return {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${user}`,
            Accept: 'application/json',
            'CSRF-Token': csrfToken,
            'X-CSRF-Token': csrfToken
        };
    } else {
        return {};
    }
}
