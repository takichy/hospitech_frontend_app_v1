# pull the base image
FROM node:14.17.0-alpine3.13

VOLUME [ "/app" ]

# set the working direction
WORKDIR /app

# install packages
RUN yarn cache clean

# Default command
CMD yarn install && yarn start